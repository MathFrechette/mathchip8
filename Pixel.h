#ifndef PIXEL_H 
#define PIXEL_H 

#include <SDL.h>

class Pixel
{
	#define NOIR  0 
	#define BLANC 1 
	#define LARGEUR 64 //nombre de pixels suivant la largeur 
	#define LONGUEUR 32 //nombre de pixels suivant la longueur 
	#define DIMENSIONPIXEL 8  // Dimension d'un pixel carr� de c�t� 8
		// L'�cran ayant 64 * 32 pixel et on a d�termin� que le pixel a 8 de c�t�
		// Donc pour trouver les dimensions de l'�cran, on multiplie la largeur et la longeur par la dimension du pixel
	#define LARGEUR_ECRAN   LARGEUR*DIMENSIONPIXEL      // La largeur de l'�cran 
	#define HAUTEUR_ECRAN  LONGUEUR*DIMENSIONPIXEL       // La hauteur de l'�cran 

	/*
	  Info sur la machine : La r�solution de l'�cran est de 64 � 32 pixels, et la couleur est monochrome.
	*/

public:

	// Repr�sente un pixel
	typedef struct
	{
		SDL_Rect position; //regroupe l'abscisse et l'ordonn�e 
		Uint8 couleur;   //comme son nom l'indique, c'est la couleur 
	} PIXEL;

	SDL_Window* ecran;
	SDL_Renderer* renderer;
	SDL_Texture* fondEcran;

	//SDL_Surface *carre[2];
	SDL_Surface* pixelNoir;
	SDL_Surface* pixelBlanc;
	SDL_Texture *carre[2];
	
	PIXEL pixel[LARGEUR][LONGUEUR];
	SDL_Event evenement; //pour g�rer la pause 

	Pixel();
	void initialiserEcran();
	void initialiserPixel();
	void dessinerPixel(PIXEL pixel);
	void effacerEcran();
	void updateEcran();
};

#endif