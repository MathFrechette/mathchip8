// MathChip8.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//
#include "pch.h"
#include <iostream>
#include <SDL.h>

#include <chrono>
#include <thread>

//#include "cpu.h"
#include "Pixel.h"

//using namespace std;


void quitterSDL(Pixel myPixel)
{
	SDL_FreeSurface(myPixel.pixelNoir);
	SDL_FreeSurface(myPixel.pixelBlanc);
	SDL_DestroyRenderer(myPixel.renderer);
	SDL_Quit();
}

void pause(Pixel myPixel)
{

	Uint8 continuer = 1;

	do
	{
		SDL_WaitEvent(std::addressof(myPixel.evenement));

		switch (myPixel.evenement.type)
		{
		case SDL_QUIT:
			continuer = 0;
			quitterSDL(myPixel);
			break;
		case SDL_KEYDOWN:
			continuer = 0;
			break;
		default: break;
		}
	} while (continuer == 1);

}

void initialiserSDL(Pixel myPixel)
{
	//atexit(quitterSDL(myPixel));

	if (SDL_Init(SDL_INIT_VIDEO) == -1)
	{
		fprintf(stderr, "Erreur lors de l'initialisation de la SDL %s", SDL_GetError());
		exit(EXIT_FAILURE);
	}
}

int main(int argc, char * argv[])
{
	Pixel myPixel;
	initialiserSDL(myPixel);
	myPixel.initialiserEcran();
	myPixel.initialiserPixel();

	myPixel.updateEcran();

	pause(myPixel);

	return 0;

}






