#include "pch.h"
#include "Pixel.h"

//Pixel::Pixel(){}

Pixel::Pixel()
{
}

void Pixel::initialiserEcran()
{
	ecran = NULL;
	carre[0] = NULL;
	carre[1] = NULL;

	ecran = SDL_CreateWindow("Ma fenetre de jeu",
		SDL_WINDOWPOS_UNDEFINED, //SDL_WINDOWPOS_CENTERED
		SDL_WINDOWPOS_UNDEFINED,
		LARGEUR_ECRAN, HAUTEUR_ECRAN,
		SDL_WINDOW_OPENGL);
	//SDL_WM_SetCaption("BC-Chip8 By BestCoder", NULL);
	//ecran = SDL_SetVideoMode(LARGEUR_ECRAN, HAUTEUR_ECRAN, 32, SDL_HWSURFACE);

	// We must call SDL_CreateRenderer in order for draw calls to affect this window.
	renderer = SDL_CreateRenderer(ecran, -1, 0);

	fondEcran = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, LARGEUR_ECRAN, HAUTEUR_ECRAN);

	if (ecran == NULL)
	{
		//fprintf(stderr, "Erreur lors du chargement du mode vid�o %s", SDL_GetError());
		exit(1);
	}

	// Cr�ation du pixel noir
	pixelNoir = SDL_CreateRGBSurface(0, DIMENSIONPIXEL, DIMENSIONPIXEL, 32, 0, 0, 0, 0);
	SDL_FillRect(pixelNoir, NULL, SDL_MapRGB(pixelNoir->format, 0x00, 0x00, 0x00)); //le pixel noir 

	carre[0] = SDL_CreateTextureFromSurface(renderer, pixelNoir);
	if (carre[0] == NULL)
	{
		//fprintf(stderr, "Erreur lors du chargement de la surface %s", SDL_GetError());
		exit(1);
	}

	
	


	pixelBlanc = SDL_CreateRGBSurface(0, DIMENSIONPIXEL, DIMENSIONPIXEL, 32, 0, 0, 0, 0);
	/*pixelBlanc = SDL_CreateTexture(renderer,
		SDL_PIXELFORMAT_ARGB8888,
		SDL_TEXTUREACCESS_STATIC,
		DIMENSIONPIXEL, DIMENSIONPIXEL);*/
	SDL_FillRect(pixelBlanc, NULL, SDL_MapRGB(pixelBlanc->format, 0, 0, 255)); //le pixel noir 

	carre[1] = SDL_CreateTextureFromSurface(renderer, pixelBlanc);
	//carre[1] = SDL_CreateRGBSurface(0, DIMENSIONPIXEL, DIMENSIONPIXEL, 32, 0, 0, 0, 0); //le pixel blanc 
	if (carre[1] == NULL)
	{
		//fprintf(stderr, "Erreur lors du chargement de la surface %s", SDL_GetError());
		exit(1);

		//SDL_FillRect(carre[1], NULL, SDL_MapRGB(carre[1]->format, 0xFF, 0xFF, 0xFF));  //le pixel blanc 

	}
}

void Pixel::initialiserPixel()
{

	Uint8 x = 0, y = 0;

	for (x = 0; x < LARGEUR; x++)
	{
		for (y = 0; y < LONGUEUR; y++)
		{
			pixel[x][y].position.x = x * DIMENSIONPIXEL;
			pixel[x][y].position.y = y * DIMENSIONPIXEL;
			pixel[x][y].position.w = DIMENSIONPIXEL;
			pixel[x][y].position.h = DIMENSIONPIXEL;

			if (x % (y + 1) == 0)
				pixel[x][y].couleur = NOIR;
			else
				pixel[x][y].couleur = BLANC;
		}
	}
}

void Pixel::dessinerPixel(PIXEL pixel)
{
	SDL_Rect SrcR;

	SrcR.x = 0;
	SrcR.y = 0;
	SrcR.w = DIMENSIONPIXEL;
	SrcR.h = DIMENSIONPIXEL;

	SDL_RenderDrawRect(renderer, &pixel.position);
	if (pixel.couleur == NOIR) 
	{
		SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);
	}
	else
	{
		SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0x00);
	}
	SDL_RenderFillRect(renderer, &pixel.position);

	//SDL_BlitSurface(carre[pixel.couleur], NULL, ecran, &pixel.position);
}

void Pixel::effacerEcran()
{
	//Pour effacer l'�cran, on remet tous les pixels en noir 
	/*Uint8 x = 0, y = 0;
	for (x = 0; x < LARGEUR; x++)
	{
		for (y = 0; y < LONGUEUR; y++)
		{
			pixel[x][y].couleur = NOIR;
		}
	}

	//on repeint l'�cran en noir 
	SDL_FillRect(ecran, NULL, NOIR);
	*/

	// On efface l'�cran en remettant tout en noir
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);
	SDL_RenderPresent(renderer);

}

void Pixel::updateEcran()
{
	SDL_SetRenderTarget(renderer, fondEcran);
	SDL_SetRenderDrawColor(renderer, 0x00, 0xFF, 0x00, 0x00);
	SDL_RenderClear(renderer);
	
	//On dessine tous les pixels � l'�cran 
	Uint8 x = 0, y = 0;

	for (x = 0; x < LARGEUR; x++)
	{
		for (y = 0; y < LONGUEUR; y++)
		{
			dessinerPixel(pixel[x][y]);
		}
	}
	
	SDL_SetRenderTarget(renderer, NULL);
	SDL_RenderCopy(renderer, fondEcran, NULL, NULL);
	SDL_RenderPresent(renderer);
}

