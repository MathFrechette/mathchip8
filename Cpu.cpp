#include "pch.h"
#include "cpu.h" 
#include <stdint.h>

void initialiserCpu() 
{ 
  //On initialise le tout 
	
    uint16_t i=0; 

    for(i=0;i<TAILLEMEMOIRE;i++)
    { 
        cpu.memoire[i]=0; 
    } 

     for(i=0;i<16;i++) 
     { 
        cpu.V[i]=0; 
        cpu.saut[i]=0; 
     } 

    cpu.pc=ADRESSEDEBUT; 
    cpu.nbrsaut=0; 
    cpu.compteurJeu=0; 
    cpu.compteurSon=0; 
    cpu.I=0; 

} 


void decompter() 
{ 
    if(cpu.compteurJeu>0) 
    cpu.compteurJeu--; 

    if(cpu.compteurSon>0) 
    cpu.compteurSon--; 
}