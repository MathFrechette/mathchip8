#ifndef CPU_H 
#define CPU_H 

#include <stdint.h>


// la m�moire est en octets (8 bits), soit un tableau de 4096 Uint8.
#define TAILLEMEMOIRE 4096 
#define ADRESSEDEBUT 512 


typedef struct
{
	// La m�moire est utilis�e pour charger les jeux(roms) et pour la gestion des p�riph�riques de la machine.
	uint8_t memoire[TAILLEMEMOIRE];
	
	// La Chip 8 comporte 16 registres de 8 bits dont les noms vont de V0 � VF (F = 15, en hexad�cimal). 
	// Le registre VF est utilis� pour toutes les retenues lors des calculs.
	uint8_t V[16];
	// Le registre d'adresse, qui est de 16 bits, est utilis� avec plusieurs opcodes qui impliquent des op�rations de m�moire.
	uint16_t I; //stocke une adresse m�moire ou dessinateur 

	uint16_t saut[16]; //pour g�rer les sauts dans � m�moire �, 16 au maximum 
	uint8_t nbrsaut; //stocke le nombre de sauts effectu�s pour ne pas d�passer 16 
	
	// 2 compteurs cadenc�s � 60 hertz
	uint8_t compteurJeu; //compteur pour la synchronisation 
	uint8_t compteurSon; //compteur pour le son 
	uint16_t pc; //pour parcourir le tableau � m�moire � 
} CPU;

CPU cpu;  //d�claration de notre CPU 

void initialiserCpu();
void decompter();

#endif